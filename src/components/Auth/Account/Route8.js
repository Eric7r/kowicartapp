import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet } from "react-native";
import IndexRoute8 from "./IndexRoute8";
import { API_URL } from "../../../utils/constanst";

export default function Route8(props) {
  const { Index1 } = props;

  return (
    <View style={styles.container}>
      <IndexRoute8/>
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    padding: 10,
    marginTop: 20,
  },

  title: {
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 10,
  },
});
