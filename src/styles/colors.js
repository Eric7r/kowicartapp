const colors ={
    primary:"#ff7514",
    dark:"#000",
    //fonts
    fontLight: "#fff",
    //Background
    bgLight: "#fff",
    bgDark: "#242424",
}

export default colors;