import React, {useState,useEffect,useMemo} from 'react';
import {
    StyleSheet,
    View,
    Text,
    Linking,
    SafeAreaView
  } from "react-native";
  import { map } from "lodash";
import { API_URL } from "../../utils/constanst";
import jwtDecode from "jwt-decode";
import {getTokenApi} from "../../api/token";
import { Button,List,FAB   } from "react-native-paper";
import { ScrollView } from 'react-native-gesture-handler';



export default function PedidosActivos() {

    const [pedidos, setPedidos] = useState(null);
    const [userid, setIdUser] = useState(undefined);
    const [phoneEjecutivo, setPhoneEjecutivo] = useState(0);

    useEffect(() => {
        (async () => {
          const token = await getTokenApi();
      
          if (token) {
            setIdUser({
            userid:jwtDecode(token).sub,
            });
          } else {
            setIdUser(null);
          }
      
          const url = `${API_URL}/api/pedidosActivos/${jwtDecode(token).sub}`
          fetch(url).then((response) => response.json()).then((responseJson) => {
            let dataSource = [];
            Object.values(responseJson).forEach(item => {
              dataSource = dataSource.concat(item);
            });
            setPedidos(dataSource);
        });
        })();
      }, []);

      useEffect(() => {
        (async () => {
          const token = await getTokenApi();
          if (token) {
            setIdUser({
              userid:jwtDecode(token).sub,
            });
          } else {
            setIdUser(null);
          }
      
          const url = `${API_URL}/api/numeroEjuctivo/${jwtDecode(token).sub}`
          fetch(url).then((response) => response.json()).then((responseJson) => {
            let dataSource = [];
            Object.values(responseJson).forEach(item => {
                dataSource = dataSource.concat(item);
            });
            dataSource.map((item) => {
                setPhoneEjecutivo(item.phone);
              })
        });
        })();
      }, []);

  return (
    <SafeAreaView>
      <ScrollView>
      <FAB
        style={styles.fab}
        small
        animated={true}
        color={'white'}
        icon="whatsapp"
        onPress={() => {
          Linking.openURL(
            'http://api.whatsapp.com/send?phone=+52' + phoneEjecutivo
          );
        }}
      />
        <View style={styles.container}>
          <View>
            <Text style={styles.title}>Pedidos Pendientes</Text>
          </View>
            {map(pedidos, (itemPedido,index) => (
            <View style={styles.containerProduct}  key={index}>
              <View > 
                <List.Section style={styles.product}>
                  <List.Accordion
                    theme={{ colors: { primary: '#ff6900' } }}
                    style={{backgroundColor:'white'}}
                    title={"Pedido:"+itemPedido.id}
                    left={props => <List.Icon {...props} icon="truck" />}>
                      <Text style={styles.name}>
                        <Text  style={{color:'#ff6900'}}> Fecha entrega:</Text> {itemPedido.fechaE}
                      </Text>
                      <Text style={styles.name}>
                        <Text  style={{color:'#ff6900'}}> Factura:</Text> {itemPedido.factura}
                      </Text>
                      <Text style={styles.name}>
                        <Text style={{color:'#ff6900'}}> Total:</Text> ${itemPedido.importe_modificar}
                      </Text>
                      <Text style={styles.name}>
                        <Text  style={{color:'#ff6900'}}> Productos:</Text> 
                      </Text>
                    {map(pedidos[index].productos, (itemProducto,index) => (
                        <Text style={styles.name} key={index}>
                          <Text > -{itemProducto.name}</Text> 
                        </Text>
                      ))}
                  </List.Accordion>
                </List.Section>
              </View>
            </View>
            ))}
        </View>
      </ScrollView>
    </SafeAreaView>
    
  );
}



const styles = StyleSheet.create({
    container: {
        padding: 20,
        paddingBottom: 50,
    },
    containerProduct: {
      width: "100%",
    },
    product: {
      backgroundColor: "white",
      padding: 10,
      borderRadius: 20,
      shadowColor: "black",
      shadowOffset: {
          width: 0,
          height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 10,
    },
    name: {
      marginTop: 2,
      fontSize: 14,
      right:40
    },
    title:{
        fontWeight: "bold",
        fontSize: 20,
        marginBottom: 10,
        alignSelf:"center",
        color:"#ff6900"
       },
       fab: {
        position: 'absolute',
        margin: 15,
        right: 0,
        marginBottom:10,
        backgroundColor: "#25D366"
      },
  });
  