import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet } from "react-native";
import IndexRoute3 from "./IndexRoute3";
import { API_URL } from "../../../utils/constanst";

export default function Route3(props) {
  const { Index1 } = props;

  return (
    <View style={styles.container}>
      {
        (Index1 && (
          <IndexRoute3 Index1={Index1}/>
        ))
      }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    marginTop: 20,
  },

  title: {
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 10,
  },
});
