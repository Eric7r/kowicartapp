import React from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
} from "react-native";
import { map } from "lodash";
import { useNavigation } from "@react-navigation/native";
import { API_URL_IMAGES } from "../../utils/constanst";
import { Button } from "react-native-paper";


export default function ListProduct(props) {
  const { products, idUsuario } = props;
  const navigation = useNavigation();

  const goToProduct = (idproduct) => {
    navigation.push("producto", { idproduct: idproduct });

  };

  return (
    <View style={styles.container}>
      {map(products, (product) => (
          <View style={styles.containerProduct}  key={product.idproduct}>
            <View style={styles.product}> 
              <Image
                style={styles.image}
                source={{
                  uri: `${API_URL_IMAGES}${product.image}`,
                }}
              />
              <Text style={styles.name} numberOfLines={1} ellipsizeMode="tail">
                {product.nameproduct}
              </Text>
              <Button
                    style={{                   
                    width: "100%",                  
                    borderRadius: 15,
                    borderWidth: 1,
                    backgroundColor: "#ff6900",
                    }}
                    icon="import"
                    mode="contained"
                    uppercase={false}
                    onPress={() => goToProduct(product.idproduct)}
                >
        <Text style={{ color: "white", fontSize:12 }}> Ver detalle</Text>
      </Button>
            </View>
          </View>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
    margin: -5,
  },
  containerProduct: {
    width: "50%",
    padding: 4,
  },
  product: {
    backgroundColor: "white",
    padding: 10,
    borderRadius: 20,
    shadowColor: "black",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
  image: {
    height: 150,
    resizeMode: "contain",
  },
  name: {
    fontSize: 12,
  },
});
