import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import colors from "../styles/colors";
import ChangeName from "../../src/screens/Account/ChangeName";
import EditUser from "../../src/screens/Account/EditUser";
import EditUser2 from "../../src/screens/Account/EditUser2";
import EditUser3 from "../../src/screens/Account/EditUser3";
import EditUser4 from "../../src/screens/Account/EditUser4";
import EditUser5 from "../../src/screens/Account/EditUser5";
import EditUser6 from "../../src/screens/Account/EditUser6";
import ReportError from "../../src/screens/Account/ReportError";
import EditUser7 from "../../src/screens/Account/EditUser7";
import EditUser8 from "../../src/screens/Account/EditUser8";
import Account from "../screens/Account/Account";
import delete2 from "../../src/screens/Account/delete2";
import ChatScreen from "../screens/Account/ChatScreen";
const Stack = createStackNavigator();

export default function AccountStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: colors.fontLight,
        headerStyle: { backgroundColor: colors.bgDark },
        cardStyle: {
          backgroundColor: colors.bgLight,
        },
      }}
    >
      <Stack.Screen
        name="account"
        component={Account}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="ChangeName"
        component={ChangeName}
        options={{ title: "Cambiar nombre " }}
      />
      <Stack.Screen
        name="EditUser"
        component={EditUser}
        options={{ title: "Cambiar nombre" }}
      />
      <Stack.Screen
        name="EditUser2"
        component={EditUser2}
        options={{ title: "Cambiar correo" }}
      />
      <Stack.Screen
        name="EditUser3"
        component={EditUser3}
        options={{ title: "Cambiar nombre de usuario" }}
      />
      <Stack.Screen
        name="EditUser4"
        component={EditUser4}
        options={{ title: "Cambiar direccion" }}
      />
      <Stack.Screen
        name="EditUser5"
        component={EditUser5}
        options={{ title: "Cambiar contraseña" }}
      />

      <Stack.Screen
        name="EditUser8"
        component={ChatScreen}
        options={{ title: "Mensajes" }}
      />
      
      <Stack.Screen
        name="EditUser7"
        component={EditUser7}
        options={{ title: "Historial de compras" }}
      />
    <Stack.Screen
        name="ReportError"
        component={ReportError}
        options={{ title: "Reportar error" }}
      />

      <Stack.Screen
        name="DeleteProduct"
        component={delete2}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}
