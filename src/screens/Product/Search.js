import React, { useState, useEffect } from "react";
import { ScrollView, View, Text } from "react-native";
import { size } from "lodash";
import StatusBar from "../../components/StatusBar";
import ScreenLoading from "../../components/ScreenLoading";
import ResultNotFound from "../../components/Search/ResultNotFound";
import ProductList from "../../components/Product/ProductList";
import Search from "../../components/Search";
import { searchProductsApi } from "../../api/product";
import colors from "../../styles/colors";
import jwtDecode from "jwt-decode";
import { getTokenApi } from "../../api/token";

export default function SearchScreen(props) {
  const { route } = props;
  const { params } = route;
  const [products, setProducts] = useState(null);
  const [idUser, setIdUser] = useState(undefined);

  useEffect(() => {
    (async () => {
      const token = await getTokenApi();

      if (token) {
        setIdUser({
          userid: jwtDecode(token).sub,
        });
      } else {
        setIdUser(null);
      }
      console.log("====BUSQUEDA====");
      console.log(params);
      const response = await searchProductsApi(params.search,jwtDecode(token).sub);
      setProducts(response);
    })();
  }, []);

  return (
    <>
      <StatusBar backgroundColor={colors.bgDark} barStyle="light-content" />
      <Search currentSearch={params.search} />
      {!products ? (
        <ScreenLoading text="Buscando productos" />
      ) : size(products) === 0 ? (
        <ResultNotFound search={params.search} />
      ) : (
        <ScrollView>
          <ProductList products={products} />
        </ScrollView>
      )}
    </>
  );
}
