import React, {useState,useEffect,useMemo} from 'react';
import { View, Text, StyleSheet } from 'react-native';
import IndexRoute7 from "./IndexRoute7";
import jwtDecode from "jwt-decode";
import {getTokenApi} from "../../../api/token";
import { API_URL } from "../../../utils/constanst"
import { useIsFocused } from '@react-navigation/native';
export default function Route1() {

    const [Index1, setIndex1] = useState(null);
    const [idUser, setIdUser] = useState(undefined);
    const isFocused = useIsFocused();

    useEffect(() => {
        (async () => {
          const token = await getTokenApi();
      
          if (token) {
      
            setIdUser({
              userid:jwtDecode(token).sub,
           
            });
          } else {
            setIdUser(null);
          }
           const url = `${API_URL}/api/historialpedidos/${jwtDecode(token).sub}`
      
          fetch(url).then((response) => response.json()).then((responseJson) => {
            let dataSource = [];
      
            Object.values(responseJson).forEach(item => {
                dataSource = dataSource.concat(item);
            });
      
            setIndex1(dataSource);
            
          
        });      
        })();
      }, [isFocused]);
    
    
    return (
        <View style={styles.container}>
            {Index1 && <IndexRoute7 Index1={Index1} />}
        </View>
    )
}


const styles = StyleSheet.create({

    container: {
        padding: 10,
        marginTop: 20,
    },

    title: {
        fontWeight: "bold",
        fontSize: 20,
        marginBottom: 10,
    },

});