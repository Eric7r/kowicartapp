import React, { useState, useEffect } from "react";
import {
  Text,
  Modal,
  View,
  Button,
  ScrollView,
  StyleSheet,
  CheckBox,
  TextInput,
  Platform,
  TouchableOpacity,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
} from "react-native";
import Icon from "@expo/vector-icons/AntDesign";
import { useNavigation } from "@react-navigation/native";

export default function Congratulation() {
  const navigation = useNavigation();

  return (
    <ScrollView style={styles.container}>
      <View>
        <Text
          style={{
            fontSize: 25,
            alignSelf: "center",
            color: "#808080",
            fontWeight: "600",
            marginTop: 60,
          }}
        >
          Producto agregado a tu carrito
        </Text>
      </View>
      <TouchableOpacity
        style={{ alignSelf: "center", marginTop: 30 }}
        onPress={() => navigation.navigate("home")}
      >
        <Icon name="checkcircleo" color="#1DCF11" size={120} />
      </TouchableOpacity>
      <TouchableOpacity
        style={{ alignSelf: "center", marginTop: 0 }}
        onPress={() => navigation.navigate("home")}
      >
        <Text
          style={{
            fontSize: 16,
            alignSelf: "center",
            color: "#808080",
            fontWeight: "600",
            marginTop: 10,
          }}
        >
          Presione aquí para volver
        </Text>
      </TouchableOpacity>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    paddingBottom: 50,
  },
  title: {
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 20,
    color: "#fff",
  },
});
