import React from "react";
import { StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import AwesomeIcon from "react-native-vector-icons/FontAwesome";
import colors from "../styles/colors";
import ProductStack from "./ProductStack";
import AccountStack from "./AccountStack";
import CartStack from "./CartStack";
import PedidosStack from "./PedidosStack";
import Cart from "../screens/Cart";
import Account from "../screens/Account/Account";
import Carrodecompras3 from "../screens/Account/Carrodecompras3";

const Tab = createMaterialBottomTabNavigator();

export default function AppNavigation() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        barStyle={styles.navigation}
        screenOptions={({ route }) => ({
          tabBarIcon: (routeStatus) => {
            return setIcon(route, routeStatus);
          },
        })}
      >
        <Tab.Screen
          name="home"
          component={ProductStack}
          options={{
            title: "Inicio",
          }}
        />
        <Tab.Screen
          name="cart"
          component={CartStack}
          options={{
            title: "Carrito",
          }}
        />
        <Tab.Screen
          name="account"
          component={AccountStack}
          options={{
            title: "cuenta",
          }}
        />
        <Tab.Screen
          name="pedidos"
          component={PedidosStack}
          options={{
            title: "Pedidos",
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

function setIcon(route, routeStatus) {
  let iconName = "";
  switch (route.name) {
    case "home":
      iconName = "home";
      break;
    case "cart":
      iconName = "shopping-cart";
      break;
    case "account":
      iconName = "user";
      break;
      case "pedidos":
      iconName = "list";
      break;

    default:
      break;
  }
  return <AwesomeIcon name={iconName} style={[styles.icon]} />;
}

const styles = StyleSheet.create({
  navigation: {
    backgroundColor: colors.bgDark,
  },
  icon: {
    fontSize: 20,
    color: colors.fontLight,
  },
});
