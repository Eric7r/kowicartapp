import React, { useState, useEffect } from "react";
import {
  Text,
  Modal,
  View,
  Button,
  ScrollView,
  StyleSheet,
  CheckBox,
  TextInput,
  Platform,
  TouchableOpacity,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
} from "react-native";
import ScreenLoading from "../../components/ScreenLoading";
import { getUserEditApi } from "../../api/product";
import Route7 from "../../components/Auth/Account/Route7";
import jwtDecode from "jwt-decode";
import { getTokenApi } from "../../api/token";

export default function EditUser7(props) {
  

  return (
    <>     
        <Route7/>     
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    paddingBottom: 50,
  },
  title: {
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 20,
    color: "#fff",
  },
});
