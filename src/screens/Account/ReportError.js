import React, { useState, useEffect } from "react";
import {
  Text,
  Modal,
  View,
  Button,
  ScrollView,
  StyleSheet,
  CheckBox,
  TextInput,
  Platform,
  TouchableOpacity,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
} from "react-native";
import ScreenLoading from "../../components/ScreenLoading";
import { getUserEditApi } from "../../api/product";
import Route5 from "../../components/Auth/Account/Route5";
import jwtDecode from "jwt-decode";
import { getTokenApi } from "../../api/token";

export default function ReportError() {
  const [Index1, setIndex1] = useState(null);
  const [idUser, setIdUser] = useState(undefined);
  useEffect(() => {
    (async () => {
      const token = await getTokenApi();

      if (token) {
        setIdUser({
          userid: jwtDecode(token).sub,
        });
      } else {
        setIdUser(null);
      }

      const response = await getUserEditApi(jwtDecode(token).sub);
      setIndex1(response);
    })();
  }, []);

  return (
    <>
      {!Index1 ? (
        <ScreenLoading text="Cargando por favor espere" size="large" />
      ) : (
        <Route5 Index1={Index1} />
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    paddingBottom: 50,
  },
  title: {
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 20,
    color: "#fff",
  },
});
