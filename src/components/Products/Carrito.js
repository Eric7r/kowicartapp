import React, { useState, useEffect, useCallback } from "react";
import {
  Image,
  TouchableHighlight,
  Text,
  Modal,
  View,
  Button,
  ScrollView,
  StyleSheet,
  CheckBox,
  TextInput,
  Platform,
  TouchableOpacity,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
  Picker,
  TouchableWithoutFeedback,
  ToastAndroid,
} from "react-native";
import Icon3 from "@expo/vector-icons/FontAwesome";
import { map } from "lodash";
import { useNavigation } from "@react-navigation/native";
import Icon from "@expo/vector-icons/Fontisto";
import { API_URL_IMAGES } from "../../utils/constanst";
import { RadioButton } from "react-native-paper";
import { useFormik } from "formik";
import * as Yup from "yup";
import Toast from "react-native-root-toast";
import { RootSiblingParent } from "react-native-root-siblings";
import { updateCarrito } from "../../api/product";
import { useFocusEffect } from "@react-navigation/native";
import DatePicker from "react-native-datepicker";
import { getTokenApi } from "../../api/token";
import useAuth from "../../hooks/useAuth";

import { NavigationEvents } from "@react-navigation/native";

export default function Carrito(props) {
  const { Index1 } = props;
  const { Index2 } = props;
  const { Index3 } = props;

  console.log("=====INDEX1========")
  console.log(Index1)
  console.log("=====INDEX2========")
  console.log(Index2)
  console.log("=====INDEX3========")
  console.log(Index3)
  const [isSelected, setSelection] = useState(false);
  const [loading, setLoading] = useState(false);
  const navigation = useNavigation();
  //console.log("INDEX1"+JSON.stringify(Index1));
  console.log(Index2);
  //console.log(Index3);
  const [idUsuario, setIdUsuario] = useState(null);

  useEffect(() => {
    (async () => {
      const token = await getTokenApi();
      if (token) {
        setIdUsuario(token);
      } else {
        setIdUsuario(null);
      }
    })();
  }, []);

  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async (formData) => {
      setLoading(true);
      try {
        const response = await updateCarrito(formData);

        if (response.statusCode === 200) {
          if (Index1.usuarios.organization_id == null) {
            if (Platform.OS === "android") {
              ToastAndroid.show(
                "Pedido realizado con exito, Su usuario está siendo dado de alta",
                ToastAndroid.SHORT
              );
            } else {
              Toast.show(
                "Pedido realizado con exito, Su usuario está siendo dado de alta",
                { position: Toast.positions.CENTER }
              );
            }
          } else {
            if (Platform.OS === "android") {
              ToastAndroid.show(
                "Pedido realizado con exito",
                ToastAndroid.SHORT
              );
            } else {
              Toast.show("Pedido realizado con exito", {
                position: Toast.positions.CENTER,
              });
            }
          }
        } else {
          if (Platform.OS === "android") {
            ToastAndroid.show(
              "La fecha no puede ser domingo y debe ser mayor a 48 hrs",
              ToastAndroid.SHORT
            );
          } else {
            Toast.show(
              "La fecha no puede ser domingo y debe ser mayor a 48 hrs",
              { position: Toast.positions.CENTER }
            );
          }
        }
      } catch (error) {
        setLoading(false);
      }
    },
  });

  const [date, setDate] = useState(new Date());
  const [selectedValue, setSelectedValue] = useState("20");

  return (
    <ScrollView style={{ width: "100%" }}>
      <View style={styles.container}>
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#fff",
          }}
        >
          {/* Titulo principal */}
          <Text
            style={{
              fontSize: 25,
              alignSelf: "center",
              color: "#ff6900",
              fontWeight: "700",
              marginTop: 10,
            }}
          >
            Carrito de compras
          </Text>

          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginHorizontal: 15,
              marginTop: 15,
              paddingHorizontal: 10,
              paddingVertical: 2,
              backgroundColor: "#fff",
              borderBottomWidth: 1,
              borderBottomColor: "#D8D8D8",
            }}
          >
            {/* TH */}
            <Text style={{ width: "33%", fontSize: 10, color: "#333333" }}>
              Nombre
            </Text>
            <Text style={{ width: "15%", fontSize: 10, color: "#333333" }}>
              Precio
            </Text>
            <Text style={{ width: "20%", fontSize: 10, color: "#333333" }}>
              Cantidad
            </Text>
            <Text style={{ width: "10%", fontSize: 10, color: "#333333" }}>
              Iva
            </Text>
            <Text style={{ width: "15%", fontSize: 10, color: "#333333" }}>
              Subtotal
            </Text>
            <Text style={{ width: "15%", fontSize: 10, color: "#333333" }}>
              Borrar
            </Text>
          </View>

          
          <View style={{ flex: 1, padding: 10, alignSelf: "center" }}>
            {map(Index2, (Indexs2) => (
              <React.Fragment key={Indexs2.id_cart_details}>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    paddingVertical: 2,
                    width: "100%",
                  }}
                >
                  <Text
                    style={{
                      width: "30%",
                      height: 50,
                      fontSize: 10,
                      color: "#7E7E7E",
                      alignSelf: "center",
                    }}
                  >
                    {Indexs2.name_short}
                  </Text>
                  <Text
                    style={{
                      width: "15%",
                      height: 50,
                      fontSize: 10,
                      color: "#7E7E7E",
                      alignSelf: "center",
                    }}
                  >
                    $ {Indexs2.precio_producto}
                  </Text>
                  <Text
                    style={{
                      width: "15%",
                      height: 50,
                      fontSize: 10,
                      color: "#7E7E7E",
                    }}
                  >
                    {Indexs2.quantity}KG
                  </Text>
                  <Text
                    style={{
                      width: "10%",
                      height: 50,
                      fontSize: 10,
                      color: "#7E7E7E",
                    }}
                  >
                    $ {Indexs2.iva}
                  </Text>
                  <Text
                    style={{
                      width: "15%",
                      height: 50,
                      fontSize: 10,
                      color: "#7E7E7E",
                    }}
                  >
                    $ {Indexs2.importe}
                  </Text>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      navigation.navigate("SearchProduct", {
                        paramKey3: Indexs2.id_cart_details,
                      })
                    }
                  >
                    <Text
                      style={{
                        width: "10%",
                        height: 50,
                        fontSize: 15,
                        color: "red",
                        marginTop: 0,
                      }}
                    >
                      X
                    </Text>
                  </TouchableWithoutFeedback>
                </View>
              </React.Fragment>
            ))}

            <Picker
              selectedValue={selectedValue}
              onValueChange={(itemValue, itemIndex) =>
                setSelectedValue(itemValue)
              }
              value={(formik.values.direccionselect = selectedValue)}
              error={formik.errors.direccionselect}
              style={{ marginBottom: 10 }}
            >
              <Picker.Item label="Direccion de entrega" value="20" />
              {map(Index3, (Indexs3) => (
                <Picker.Item
                  label={Indexs3.direccion}
                  value={Indexs3.direccion}
                  key={Indexs3}
                />
              ))}
            </Picker>

            <View
              style={{
                alignItems: "center",
                marginTop: 0,
                paddingVertical: 2,
                width: "100%",
                alignSelf: "center",
              }}
            >
              <DatePicker
                style={styles.datePickerStyle}
                date={date} // Initial date from state
                mode="date" // The enum of date, datetime and time
                placeholder="select date"
                format="YYYY-MM-DD"
                minDate="2016-01-01"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    //display: 'none',
                    position: "absolute",
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    marginLeft: 36,
                  },
                }}
                onDateChange={(date) => {
                  setDate(date);
                }}
                value={(formik.values.fechaE = date)}
                error={formik.errors.fechaE}
              />
              <View
                style={{
                  alignItems: "center",
                  marginTop: 0,
                  paddingVertical: 2,
                  width: "100%",
                  marginTop: 20,
                }}
              >
                <Text style={styles.label}>¿Requiere factura?</Text>

                <RadioButton.Group
                  onValueChange={(text) =>
                    formik.setFieldValue("factura", text)
                  }
                  value={formik.values.factura}
                  error={formik.errors.factura}
                >
                  <View
                    style={{
                      alignItems: "center",
                      marginTop: 0,
                      width: "100%",
                      flexDirection: "row",
                    }}
                  >
                    <View style={{ width: "25%", flexDirection: "row" }}>
                      <RadioButton value="si"></RadioButton>
                      <Text style={{ fontSize: 24 }}>SI</Text>
                    </View>
                    <View style={{ width: "25%", flexDirection: "row" }}>
                      <RadioButton value="no"></RadioButton>
                      <Text style={{ fontSize: 24 }}>No</Text>
                    </View>
                  </View>
                </RadioButton.Group>
              </View>

              <Text
                style={{
                  fontSize: 12,
                  alignSelf: "center",
                  color: "#808080",
                  fontWeight: "700",
                  marginTop: 25,
                  paddingBottom: 0,
                }}
              >
                Importe a pagar (total):
              </Text>
              <Text
                style={{
                  fontSize: 40,
                  alignSelf: "center",
                  color: "#000",
                  fontWeight: "700",
                  marginTop: 0,
                  paddingBottom: 25,
                }}
              >
                $ {Index1.carritos.importe_total}
              </Text>

              <TextInput
                onChangeText={(text) => formik.setFieldValue("id", text)}
                value={(formik.values.id = Index1.carritos.id.toString())}
                error={formik.errors.id}
                style={{
                  borderWidth: 0.5,
                  borderColor: "#808080",
                  width: "20%",
                  marginTop: -20,
                  color: "#808080",
                  textAlign: "center",
                  marginBottom: 40,
                  display: "none",
                }}
              />

              <TextInput
                onChangeText={(text) => formik.setFieldValue("tokencode", text)}
                value={(formik.values.tokencode = idUsuario)}
                error={formik.errors.tokencode}
                style={{
                  borderWidth: 0.5,
                  borderColor: "#808080",
                  width: "20%",
                  marginTop: -20,
                  color: "#808080",
                  textAlign: "center",
                  marginBottom: 40,
                  display: "none",
                }}
              />

              <TextInput
                onChangeText={(text) =>
                  formik.setFieldValue("idcentroventa", text)
                }
                value={
                  (formik.values.idcentroventa = Index1.idCentro.toString())
                }
                error={formik.errors.idcentroventa}
                style={{
                  borderWidth: 0.5,
                  borderColor: "#808080",
                  width: "20%",
                  marginTop: -20,
                  color: "#808080",
                  textAlign: "center",
                  marginBottom: 40,
                  display: "none",
                }}
              />

              <TextInput
                onChangeText={(text) => formik.setFieldValue("userid", text)}
                value={(formik.values.userid = Index1.usuarios.id.toString())}
                error={formik.errors.userid}
                style={{
                  borderWidth: 0.5,
                  borderColor: "#808080",
                  width: "20%",
                  marginTop: -20,
                  color: "#808080",
                  textAlign: "center",
                  marginBottom: 40,
                  display: "none",
                }}
              />

              {(() => {
                if (Index1.usuarios.organization_id == null) {
                  return (
                    <View>
                      <TextInput
                        onChangeText={(text) =>
                          formik.setFieldValue("organization_id", text)
                        }
                        value={(formik.values.organization_id = "1")}
                        error={formik.errors.organization_id}
                        style={{
                          borderWidth: 0.5,
                          borderColor: "#808080",
                          width: "20%",
                          marginTop: -20,
                          color: "#808080",
                          textAlign: "center",
                          marginBottom: 40,
                          display: "none",
                        }}
                      />
                    </View>
                  );
                }
                return (
                  <View>
                    <TextInput
                      onChangeText={(text) =>
                        formik.setFieldValue("organization_id", text)
                      }
                      value={
                        (formik.values.organization_id =
                          Index1.usuarios.organization_id.toString())
                      }
                      error={formik.errors.organization_id}
                      style={{
                        borderWidth: 0.5,
                        borderColor: "#808080",
                        width: "20%",
                        marginTop: -20,
                        color: "#808080",
                        textAlign: "center",
                        marginBottom: 40,
                        display: "none",
                      }}
                    />
                  </View>
                );
              })()}

              <TextInput
                onChangeText={(text) => formik.setFieldValue("city", text)}
                value={(formik.values.city = Index1.usuarios.city.toString())}
                error={formik.errors.city}
                style={{
                  borderWidth: 0.5,
                  borderColor: "#808080",
                  width: "20%",
                  marginTop: -20,
                  color: "#808080",
                  textAlign: "center",
                  marginBottom: 40,
                  display: "none",
                }}
              />
              <TextInput
                onChangeText={(text) => formik.setFieldValue("contador", text)}
                value={(formik.values.contador = Index1.wordCount2.toString())}
                error={formik.errors.contador}
                style={{
                  borderWidth: 0.5,
                  borderColor: "#808080",
                  width: "20%",
                  marginTop: -20,
                  color: "#808080",
                  textAlign: "center",
                  marginBottom: 40,
                  display: "none",
                }}
              />
              <TextInput
                onChangeText={(text) =>
                  formik.setFieldValue("count_products", text)
                }
                value={
                  (formik.values.count_products = Index1.wordCount.toString())
                }
                error={formik.errors.count_products}
                style={{
                  borderWidth: 0.5,
                  borderColor: "#808080",
                  width: "20%",
                  marginTop: -20,
                  color: "#808080",
                  textAlign: "center",
                  marginBottom: 40,
                  display: "none",
                }}
              />
              {(() => {
                /*if (Index1.wordCount2 >= 1) {
                  return <Text style={{color:"#DD0000",fontSize:16,fontWeight:'700'}}>Tiene un pedido aun en proceso</Text>;
                }*/
                return (
                  <View>
                    {(() => {
                      if (Index1.wordCount == 0) {
                        return (
                          <Text
                            style={{
                              color: "#DD0000",
                              fontSize: 16,
                              fontWeight: "700",
                            }}
                          >
                            Tiene que agregar almenos un producto
                          </Text>
                        );
                      }
                      return (
                        <View>
                          {(() => {
                            if (!formik.values.fechaE) {
                              return (
                                <View
                                  style={{
                                    alignItems: "center",
                                    justifyContent: "center",
                                    marginTop: 30,
                                    backgroundColor: "#fff",
                                    paddingVertical: 0,
                                    borderRadius: 23,
                                    width: 300,
                                  }}
                                >
                                  {/* Iniciar sesion */}
                                  <TouchableOpacity
                                    mode="contained"
                                    onPress={formik.handleSubmit}
                                    loading={setLoading}
                                    disabled={true}
                                    style={styles.buttonagregar2}
                                  >
                                    <Text
                                      style={{
                                        fontSize: 24,
                                        color: "#fff",
                                        alignSelf: "center",
                                        paddingTop: 5,
                                        fontWeight: "600",
                                      }}
                                    >
                                      Realizar pedido
                                    </Text>
                                  </TouchableOpacity>
                                </View>
                              );
                            }
                            return (
                              <View>
                                {(() => {
                                  if (!formik.values.factura) {
                                    return (
                                      <View
                                        style={{
                                          alignItems: "center",
                                          justifyContent: "center",
                                          marginTop: 30,
                                          backgroundColor: "#fff",
                                          paddingVertical: 0,
                                          borderRadius: 23,
                                          width: 300,
                                        }}
                                      >
                                        {/* Iniciar sesion */}

                                        <TouchableOpacity
                                          mode="contained"
                                          onPress={formik.handleSubmit}
                                          loading={setLoading}
                                          disabled={true}
                                          style={styles.buttonagregar2}
                                        >
                                          <Text
                                            style={{
                                              fontSize: 24,
                                              color: "#fff",
                                              alignSelf: "center",
                                              paddingTop: 5,
                                              fontWeight: "600",
                                            }}
                                          >
                                            Realizar pedido
                                          </Text>
                                        </TouchableOpacity>
                                      </View>
                                    );
                                  }
                                  return (
                                    <View>
                                      {(() => {
                                        if (
                                          formik.values.direccionselect == 20
                                        ) {
                                          return (
                                            <View
                                              style={{
                                                alignItems: "center",
                                                justifyContent: "center",
                                                marginTop: 30,
                                                backgroundColor: "#fff",
                                                paddingVertical: 0,
                                                borderRadius: 23,
                                                width: 300,
                                              }}
                                            >
                                              {/* Iniciar sesion */}

                                              <TouchableOpacity
                                                mode="contained"
                                                onPress={formik.handleSubmit}
                                                loading={setLoading}
                                                disabled={true}
                                                style={styles.buttonagregar2}
                                              >
                                                <Text
                                                  style={{
                                                    fontSize: 24,
                                                    color: "#fff",
                                                    alignSelf: "center",
                                                    paddingTop: 5,
                                                    fontWeight: "600",
                                                  }}
                                                >
                                                  Realizar pedido
                                                </Text>
                                              </TouchableOpacity>
                                            </View>
                                          );
                                        }
                                        return (
                                          <View>
                                            {(() => {
                                              if (
                                                formik.values.fechaE ==
                                                new Date()
                                              ) {
                                                return (
                                                  <View
                                                    style={{
                                                      alignItems: "center",
                                                      justifyContent: "center",
                                                      marginTop: 30,
                                                      backgroundColor: "#fff",
                                                      paddingVertical: 0,
                                                      borderRadius: 23,
                                                      width: 300,
                                                    }}
                                                  >
                                                    {/* Iniciar sesion */}

                                                    <TouchableOpacity
                                                      mode="contained"
                                                      onPress={
                                                        formik.handleSubmit
                                                      }
                                                      loading={setLoading}
                                                      disabled={true}
                                                      style={
                                                        styles.buttonagregar2
                                                      }
                                                    >
                                                      <Text
                                                        style={{
                                                          fontSize: 24,
                                                          color: "#fff",
                                                          alignSelf: "center",
                                                          paddingTop: 5,
                                                          fontWeight: "600",
                                                        }}
                                                      >
                                                        Realizar pedido
                                                      </Text>
                                                    </TouchableOpacity>
                                                  </View>
                                                );
                                              }
                                              return (
                                                <View>
                                                  <View
                                                    style={{
                                                      alignItems: "center",
                                                      justifyContent: "center",
                                                      marginTop: 30,
                                                      backgroundColor: "#fff",
                                                      paddingVertical: 0,
                                                      borderRadius: 23,
                                                      width: 300,
                                                    }}
                                                  >
                                                    <TouchableOpacity
                                                      mode="contained"
                                                      onPress={
                                                        formik.handleSubmit
                                                      }
                                                      loading={setLoading}
                                                      title="Press Me"
                                                      onPressIn={() =>
                                                        navigation.navigate(
                                                          "Carrodecompras3"
                                                        )
                                                      }
                                                      style={{
                                                        backgroundColor:
                                                          "#FF7709",
                                                        borderRadius: 23,
                                                        width: 250,
                                                        height: 45,
                                                        marginBottom: 0,
                                                      }}
                                                    >
                                                      <Text
                                                        style={{
                                                          fontSize: 24,
                                                          color: "#fff",
                                                          alignSelf: "center",
                                                          paddingTop: 5,
                                                          fontWeight: "600",
                                                        }}
                                                      >
                                                        Realizar pedido
                                                      </Text>
                                                    </TouchableOpacity>
                                                  </View>
                                                </View>
                                              );
                                            })()}
                                          </View>
                                        );
                                      })()}
                                    </View>
                                  );
                                })()}
                              </View>
                            );
                          })()}
                        </View>
                      );
                    })()}
                  </View>
                );
              })()}
            </View>
          </View>

          {/* politica 1 */}
          <Text
            style={{
              fontSize: 11,
              color: "#5C5C5C",

              textAlign: "center",
              marginHorizontal: 55,
              marginTop: 30,
              opacity: 0.8,

              marginBottom: 0,
            }}
          >
            Alimentos Kowi 2021
          </Text>

          {/* politica 2 */}
          <Text
            style={{
              fontSize: 9,
              color: "#0083CA",

              textAlign: "center",
              marginHorizontal: 55,
              marginTop: 0,
              opacity: 0.8,
              marginBottom: 40,
            }}
          >
            Politica de privacidad
          </Text>
        </View>
      </View>
    </ScrollView>
  );
}

function initialValues() {
  return {
    id: "",
    idcentroventa: "",
    userid: "",
    city: "",
    contador: "",
    count_products: "",
    fechaE: "",
    direccionselect: "",
    factura: "",
    tokencode: "",
    organization_id: "",
  };
}

function validationSchema() {
  return {
    id: Yup.string().required(true),
    idcentroventa: Yup.string().required(true),
    userid: Yup.string().required(true),
    city: Yup.string().required(true),
    contador: Yup.string().required(true),
    count_products: Yup.string().required(true),
    fechaE: Yup.string().required(true),
    direccionselect: Yup.string().required(true),
    factura: Yup.string().required(true),
    tokencode: Yup.string().required(true),
    organization_id: Yup.string().required(true),
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
    margin: -3,
    backgroundColor: "#fff",
  },
  containerProduct: {
    width: "100%",
    padding: 3,
  },
  product: {
    backgroundColor: "#f0f0f0",
    padding: 10,
  },
  image: {
    height: "100%",
    width: "50%",
    resizeMode: "contain",
  },
  name: {
    marginTop: 15,
    fontSize: 18,
  },
  buttonagregar: {
    backgroundColor: "#FF7709",
    borderRadius: 23,
    width: "100%",
    height: 40,
    marginBottom: 0,
  },
  buttonagregar2: {
    backgroundColor: "#FFC18F",
    borderRadius: 23,
    width: "100%",
    height: 40,
    marginBottom: 0,
  },
  buttonagregar3: {
    backgroundColor: "#FF7709",
    borderRadius: 23,
    width: "50%",
    height: 70,
    marginBottom: 0,
  },
});
