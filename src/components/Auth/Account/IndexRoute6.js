import React, { useState, useEffect } from "react";
import {
  Picker,
  Image,
  TouchableHighlight,
  Text,
  Modal,
  View,
  ScrollView,
  StyleSheet,
  CheckBox,
  Platform,
  TouchableOpacity,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
  ToastAndroid,
} from "react-native";
import Icon from "@expo/vector-icons/AntDesign";
import { TextInput, Button } from "react-native-paper";
import { formStyle } from "../../../styles";
import { forgot_pass } from "../../../api/product";
import { useFormik } from "formik";
import * as Yup from "yup";
import Toast from "react-native-root-toast";
import {RootSiblingParent} from "react-native-root-siblings";
import { map } from "lodash";
import { useNavigation } from "@react-navigation/native";


export default function IndexRoute5() {
  
  const navigation = useNavigation();

  const [loading, setLoading] = useState(false);
  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async (formData) => {
      setLoading(true);
      try {
        const response = await forgot_pass(formData);        
        if (Platform.OS === 'android') {
          ToastAndroid.show("Se ha enviado un link a su correo", ToastAndroid.SHORT)
        } else {
          Toast.show("Se ha enviado un link a su correo", { position: Toast.positions.CENTER, });          
        }
      } catch (error) {      
        setLoading(false);
        if (Platform.OS === 'android') {
          ToastAndroid.show("Verifique sus datos", ToastAndroid.SHORT)
        } else {
          Toast.show("Verifique sus datos", { position: Toast.positions.CENTER, });          
        }
      }
    },
  });

  return (
    <SafeAreaView>
      <ScrollView>
        <View style={{ height: "auto", marginTop: 20, marginBottom: 40 }}>
          <Text
            style={{
              fontSize: 25,
              alignSelf: "center",
              color: "#808080",
              fontWeight: "600",
              marginTop: 5,
              marginBottom: 0,
            }}
          >
           Cambiar contraseña
          </Text>
          <Text
            style={{
              fontSize: 12,
              alignSelf: "center",
              color: "#808080",
              fontWeight: "100",
              marginTop: 0,
              marginBottom: 20,
            }}
          >
            Escribe tu correo
          </Text>
       
          <TextInput
            style={styles.postInput}
            onChangeText={(text) => formik.setFieldValue("email", text)}
            value={formik.values.email}
            error={formik.errors.email}
            
            placeholder="Correo electronico"
            underlineColorAndroid="transparent"
            require={true}
          />

          {(() => {
            if (!formik.values.email) {
              return (
                <TouchableOpacity
                  mode="contained"
                  disabled={true}
                  onPress={formik.handleSubmit}
                  loading={setLoading}
                  style={styles.buttonagregar2}
                  onPressIn={() => navigation.navigate("account")}
                >
                  <Text
                    style={{
                      fontSize: 24,
                      color: "#fff",
                      alignSelf: "center",
                      paddingTop: 5,
                      fontWeight: "700",
                    }}
                  >
                    Enviar
                  </Text>
                </TouchableOpacity>
              );
            }
            return (
              <RootSiblingParent>
              <TouchableOpacity
                mode="contained"
                onPress={formik.handleSubmit}
                  loading={setLoading}
                  style={styles.buttonagregar}
                 
              >
                <Text
                  style={{
                    fontSize: 24,
                    color: "#fff",
                    alignSelf: "center",
                    paddingTop: 5,
                    fontWeight: "700",
                  }}
                >
                  Enviar
                </Text>
              </TouchableOpacity>
              </RootSiblingParent>
            );
          })()}

          <TouchableOpacity
            style={styles.buttonagregar2}
            onPress={() => navigation.navigate("account")}
          >
            <Text
              style={{
                fontSize: 24,
                color: "#fff",
                alignSelf: "center",
                paddingTop: 5,
                fontWeight: "700",
              }}
            >
              Cancelar
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

function initialValues() {
  return {
    email: "",
  };
}

function validationSchema() {
  return {
    email: Yup.string().required(true),
    
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
    margin: -3,
  },
  buttonagregar: {
    backgroundColor: "#FF7709",
    borderRadius: 23,
    width: "100%",
    height: 40,
    marginBottom: 10,
  },
  buttonagregar2: {
    backgroundColor: "#C1C1C1",
    borderRadius: 23,
    width: "100%",
    height: 40,
    marginBottom: 10,
  },
  postInput: {
    fontSize: 16,
    borderColor: "#42435b",
    borderWidth: 1,
    margin: 10,
    fontFamily: "Outrun future",
  },
});
