import React from 'react'
import { StyleSheet} from 'react-native'
import {Button} from "react-native-paper";


export default function Buy(props) {
    const {product,quantity,productExistencia,value,idCart,idUsuario} = props;

    const addProductCart = () =>{

     console.log(product);
     console.log(quantity);
     console.log(productExistencia);
     console.log(value);
     console.log(idCart);
     console.log(idUsuario);

    };

    return (
       <Button
       mode="contained"
       contentStyle={styles.btnBuyContent}
       labelStyle= {styles.btnLabel}
       style={styles.btn}
       onPress={addProductCart}
       >
       Añadir al Carrito
       </Button>
    );
}


const styles = StyleSheet.create({
    btnLabel: {
      fontSize: 18,
    },
    btn: {
      marginTop: 20,
    },
    btnBuyContent: {
      backgroundColor: "#ff7514",
      paddingVertical: 5,
    },
  });