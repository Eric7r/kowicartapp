import React, { useState, useEffect } from "react";
import {
  Picker,
  Image,
  TouchableHighlight,
  Text,
  Modal,
  View,
  ScrollView,
  StyleSheet,
  CheckBox,
  Platform,
  TouchableOpacity,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
  ToastAndroid,
} from "react-native";
import Icon from "@expo/vector-icons/AntDesign";
import { TextInput, Button } from "react-native-paper";
import { formStyle } from "../../../styles";
import { updateuser } from "../../../api/product";
import { useFormik } from "formik";
import * as Yup from "yup";
import Toast from "react-native-root-toast";
import {RootSiblingParent} from "react-native-root-siblings";
import { map } from "lodash";
import { useNavigation } from "@react-navigation/native";

export default function IndexRoute1(props) {
  const { Index1 } = props;

  const navigation = useNavigation();

  const [loading, setLoading] = useState(false);
  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async (formData) => {
      setLoading(true);
      try {
        const response = await updateuser(formData);
      
        if (Platform.OS === 'android') {
          ToastAndroid.show("Nombre cambiado correctamente", ToastAndroid.SHORT)
        } else {
          Toast.show("Nombre cambiado correctamente", { position: Toast.positions.CENTER, });          
        }
      } catch (error) {
        if (Platform.OS === 'android') {
          ToastAndroid.show("Verifique sus datos", ToastAndroid.SHORT)
        } else {
          Toast.show("Verifique sus datos", { position: Toast.positions.CENTER, });          
        }
        setLoading(false);
      }
    },
  });


  return (
    <SafeAreaView>
      <ScrollView>
        <View style={{ height: "auto", marginTop: 20, marginBottom: 40 }}>
          <Text
            style={{
              fontSize: 25,
              alignSelf: "center",
              color: "#808080",
              fontWeight: "600",
              marginTop: 5,
              marginBottom: 0,
            }}
          >
            Datos del usuario
          </Text>
          <Text
            style={{
              fontSize: 12,
              alignSelf: "center",
              color: "#808080",
              fontWeight: "100",
              marginTop: 0,
              marginBottom: 20,
            }}
          >
            Este es tu nombre actual:
          </Text>
          <TextInput
            onChangeText={(text) => formik.setFieldValue("iduser", text)}
            value={(formik.values.iduser = Index1.users.id.toString())}
            error={formik.errors.iduser}
            label="iduser"
            style={{ display: "none" }}
          />
          <TextInput
            onChangeText={(text) => formik.setFieldValue("name", text)}
            value={(formik.values.name = Index1.users.name.toString())}
            error={formik.errors.name}
            label="Nombre"
            style={{ display: "none" }}
          />

          <TextInput
            onChangeText={(text) => formik.setFieldValue("name2", text)}
            value={formik.values.name2}
            error={formik.errors.name2}
            label={Index1.users.name}
            style={formStyle.input}
          />

          {/* Iniciar sesion */}
       
          <TouchableOpacity
            mode="contained"
            onPress={formik.handleSubmit}
            loading={setLoading}
            style={styles.buttonagregar}
            onPressIn={() => navigation.navigate("account")}
          >
            <Text
              style={{
                fontSize: 24,
                color: "#fff",
                alignSelf: "center",
                paddingTop: 5,
                fontWeight: "700",
              }}
            >
              Guardar
            </Text>
          </TouchableOpacity>
          

          <TouchableOpacity
            style={styles.buttonagregar2}
            onPress={() => navigation.navigate("account")}
          >
            <Text
              style={{
                fontSize: 24,
                color: "#fff",
                alignSelf: "center",
                paddingTop: 5,
                fontWeight: "700",
              }}
            >
              Cancelar
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

function initialValues() {
  return {
    iduser: "",
    name: "",
    name2: "",
  };
}

function validationSchema() {
  return {
    iduser: Yup.string().required(true),
    name: Yup.string().required(true),
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
    margin: -3,
  },
  buttonagregar: {
    backgroundColor: "#FF7709",
    borderRadius: 23,
    width: "100%",
    height: 40,
    marginBottom: 10,
  },
  buttonagregar2: {
    backgroundColor: "#C1C1C1",
    borderRadius: 23,
    width: "100%",
    height: 40,
    marginBottom: 0,
  },
  containerProduct: {
    width: "100%",
    padding: 3,
  },
  product: {
    backgroundColor: "#f0f0f0",
    padding: 10,
  },
  image: {
    height: "80%",
    width: "100%",
    resizeMode: "contain",
    alignSelf: "center",
  },
  name: {
    marginTop: 15,
    fontSize: 18,
  },
  container2: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start", // if you want to fill rows left to right
  },
});
