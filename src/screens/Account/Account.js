import React, { useState, useCallback } from "react";
import { ScrollView } from "react-native";
import { useFocusEffect } from "@react-navigation/native";
import StatusBar from "../../components/StatusBar";
import Menu from "../../components/Auth/Account/Menu";
import ScreenLoading from "../../components/ScreenLoading";
import Search from "../../components/Search";
import UserInfo from "../../components/Auth/Account/Userinfo";
import {getUserApi} from "../../api/user";
import useAuth from "../../hooks/useAuth";
import colors from "../../styles/colors";
import { useIsFocused } from '@react-navigation/native';

export default function Account() {
  const [user, setUser] = useState(null);
  const { auth } = useAuth();

  const isFocused = useIsFocused();

  useFocusEffect(
    useCallback(() => {
      (async () => {
        const response = await getUserApi(auth.userid);
         setUser(response);
      })();
    }, [isFocused])
  );

  return (
    <>
     
         
          <ScrollView>
           
            <Menu />
          </ScrollView>
        </>
    
  );
}