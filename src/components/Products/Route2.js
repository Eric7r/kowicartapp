import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import IndexRoute2 from "./IndexRoute2";
import { API_URL } from "../../utils/constanst"
import jwtDecode from "jwt-decode";
import {getTokenApi} from "../../api/token";

export default function Route2(props) {
    const { Indexs1 } = props;
    const [Index2, setIndex2] = useState(null);
    const [idUser, setIdUser] = useState(undefined);

    useEffect(() => {
        (async () => {
          const token = await getTokenApi();
      
          if (token) {
      
            setIdUser({
              userid:jwtDecode(token).sub,
           
            });
          } else {
            setIdUser(null);
          }
           const url = `${API_URL}/api/productselected/${jwtDecode(token).sub}/${Indexs1.products.id}`
      
          fetch(url).then((response) => response.json()).then((responseJson) => {
            let dataSource = [];
      
            Object.values(responseJson).forEach(item => {
                dataSource = dataSource.concat(item);
            });
            
            setIndex2(dataSource);
           
            
          
        });      
        })();
      }, []);
    return (
        <View style={styles.container}>
            {Index2, Indexs1 && <IndexRoute2 Index2={Index2} Indexs1={Indexs1} />}
        </View>
    )
}


const styles = StyleSheet.create({

    container: {
        padding: 0,
        marginTop: 20,
    },

    title: {
        fontWeight: "bold",
        fontSize: 20,
        marginBottom: 10,
    },

});