import AsyncStorage from "@react-native-async-storage/async-storage";
import {API_URL} from "../utils/constanst";
import { TOKEN } from "../utils/constanst";
import {IDUSER} from "../utils/constanst";




export async function getTokenApi() {
    try {
      const token = await AsyncStorage.getItem(TOKEN);
      return token;
    } catch (e) {
      return null;
    }
  }


export async function setTokenApi(token) {
    try {
      await AsyncStorage.setItem(TOKEN, token);
      return true;
    } catch (e) {
      return null;
    }
  }

  export async function removeTokenApi() {
    try {
      await AsyncStorage.removeItem(TOKEN);
      return true;
    } catch (e) {
      return null;
    }
  }


  export async function getIdUserApi() {
    try {
    const idUser = await AsyncStorage.getItem(IDUSER);
      return  idUser;
    } catch (e) {
      return null;
    }
  }




export async function setIdUserApi(idUser) {
  try {
    await AsyncStorage.setItem(IDUSER, idUser);
   
    return true;
  } catch (e) {
    return null;
  }
}



export async function insertToken(Data) {
  try {
      const url = `${API_URL}/api/token`;
      const params = {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(Data),
        };
        const response = await fetch(url, params);
        const result = await response.json();
        return result;
  } catch (error) {
      console.log(error);
  return null;
  }
}

