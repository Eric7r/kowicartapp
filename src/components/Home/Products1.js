import React, {useState,useEffect,useMemo} from 'react';
import { View, Text, StyleSheet} from 'react-native';
import ListProduct from "./ListProduct";
import jwtDecode from "jwt-decode";
import ProductContext from "../../context/ProductContext"
import { API_URL_IMAGES } from "../../utils/constanst";
import { API_URL } from "../../utils/constanst";
import {getTokenApi} from "../../api/token";
import {getCartId} from "../../api/product";

export default function Products1() {

const [products, setProducts] = useState(null);
const [idUser, setIdUser] = useState(undefined);
const [idCart, setIdCart] = useState(null);

useEffect(() => {
  (async () => {
    const token = await getTokenApi();

    if (token) {

      setIdUser({
        userid:jwtDecode(token).sub,
     
      });
    } else {
      setIdUser(null);
    }

    const url = `${API_URL}/api/products1/${jwtDecode(token).sub}`

    fetch(url).then((response) => response.json()).then((responseJson) => {
      let dataSource = [];
      Object.values(responseJson).forEach(item => {
          dataSource = dataSource.concat(item);
      });
      setProducts(dataSource);
      
  });
  })();
}, []);

useEffect(() => {
  (async () =>{
      const response = await getCartId(idUser);
      setIdCart(response); 
  })()
}, [])


  const  authData = useMemo(
    () => ({idUser, }),[idUser]);
  

    return (
      <ProductContext.Provider value={authData}> 
        <View style={styles.container}> 
            <Text style={styles.title}>Listado de productos</Text>
            {products && <ListProduct products={products} idCart={idCart}/>}
        </View>
        </ProductContext.Provider>
    )
}

const styles = StyleSheet.create({

  container: {
     padding: 10,
     marginTop: 5,
  },

  title:{
   fontWeight: "bold",
   fontSize: 20,
   marginBottom: 10,
   alignSelf:"center",
   color:"#ff6900"
  },

});