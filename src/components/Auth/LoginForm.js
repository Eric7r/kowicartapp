import React, { useState } from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import { TextInput, Button } from "react-native-paper";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Linking } from "react-native";
import Toast from "react-native-root-toast";
import { RootSiblingParent } from "react-native-root-siblings";
import useAuth from "../../hooks/useAuth";

import { loginApi } from "../../api/user";
import { formStyle } from "../../styles";

export default function LoginForm(props) {
  const { changeForm } = props;
  const [loading, setLoading] = useState(false);
  const { login } = useAuth();

  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async (formData) => {
      setLoading(true);
      try {
        const response = await loginApi(formData);
        if (response.statusCode) throw response.respuesta;
        login(response);
        console.log(response)
      } catch (error) {
        console.log(error)
        Toast.show(error, {
          position: Toast.positions.CENTER,
        });
        setLoading(false);
      }
    },
  });

  return (
    <View style={styles.container}>
      <Image
        source={{ uri: "https://recetas.kowi.com.mx/Images/images/logo.png" }}
        resizeMode="contain"
        style={styles.logo}
      />
      <Text
        style={{
          fontSize: 25,
          alignSelf: "center",
          color: "#808080",
          fontWeight: "600",
          marginTop: 50,
          marginBottom: 0,
        }}
      >
        Iniciar sesión
      </Text>
      <Text
        style={{
          fontSize: 12,
          alignSelf: "center",
          color: "#808080",
          fontWeight: "100",
          marginTop: 0,
          marginBottom: 20,
        }}
      >
        ingresa tus datos
      </Text>
      <TextInput
        label="Nombre de usuario"
        style={formStyle.input}
        onChangeText={(text) => formik.setFieldValue("username", text)}
        value={formik.values.username}
        error={formik.errors.username}
      />
      <TextInput
        label="Contraseña"
        style={formStyle.input}
        secureTextEntry
        onChangeText={(text) => formik.setFieldValue("password", text)}
        value={formik.values.password}
        error={formik.errors.password}
      />
      <RootSiblingParent>
        <Button
          mode="contained"
          style={formStyle.btnSucces}
          onPress={formik.handleSubmit}
          loading={loading}
        >
          Entrar
        </Button>
      </RootSiblingParent>
      <Button
        mode="text"
        style={formStyle.btnText}
        labelStyle={formStyle.btnTextLabel}
        onPress={changeForm}
      >
        Registrarse
      </Button>
      <Button
        mode="text"
        style={formStyle.btnText}
        labelStyle={formStyle.btnTextLabel}
        onPress={() =>
          Linking.openURL("https://apprutas.kowi.com.mx/password/reset")
        }
      >
        ¿Olvidaste contraseña?
      </Button>
      
    </View>
  );
}

function initialValues() {
  return {
    username: "",
    password: "",
  };
}

function validationSchema() {
  return {
    username: Yup.string().required(true),
    password: Yup.string().required(true),
  };
}

const styles = StyleSheet.create({
  logo: {
    width: "100%",
    height: 150,
    marginTop: 45,
  },
  button: {
    backgroundColor: "#FF7709",
    borderRadius: 10,
    width: "100%",
    height: 40,
    marginBottom: 10,
  },
  container: {},
  
});
