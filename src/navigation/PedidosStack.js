import { View, Text } from 'react-native'
import React from 'react'
import colors from '../styles/colors';
import { createStackNavigator } from "@react-navigation/stack";
import PedidosActivos from "../screens/Pedidos/PedidosActivos";

const Stack = createStackNavigator();

export default function PedidosStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: colors.fontLight,
        headerStyle: { backgroundColor: colors.bgDark },
        cardStyle: {
          backgroundColor: colors.bgLight,
        },
      }}
    >
      <Stack.Screen
        name="pedidosActivos"
        component={PedidosActivos}
        options={{ headerShown: false }}
      />

  
    </Stack.Navigator>
  );
}