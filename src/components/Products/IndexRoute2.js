import React, { useState, useEffect } from "react";
import {
  Image,
  TouchableHighlight,
  Text,
  Modal,
  View,
  Button,
  ScrollView,
  StyleSheet,
  CheckBox,
  TextInput,  
  TouchableOpacity,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
  ToastAndroid,
  Platform,
} from "react-native";
import Icon from "@expo/vector-icons/AntDesign";
import Icon2 from "@expo/vector-icons/Feather";
import Icon3 from "@expo/vector-icons/FontAwesome";
import Icon4 from "@expo/vector-icons/Entypo";
import Icon5 from "@expo/vector-icons/Octicons";
import { map } from "lodash";
import { useNavigation } from "@react-navigation/native";
import { API_URL_IMAGES } from "../../utils/constanst";
import { useFormik } from "formik";
import * as Yup from "yup";
import Toast from "react-native-root-toast";
import { agregarProducto } from "../../api/product";
import { RadioButton } from "react-native-paper";
import {RootSiblingParent} from "react-native-root-siblings";

export default function IndexRoute2(props) {
  const { Index2, Indexs1 } = props;
  //console.log("INDEX1"+JSON.stringify(Indexs1));
  

  const [number, onChangeNumber] = React.useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [value, setValue] = React.useState("KG");
  const navigation = useNavigation();

  const [loading, setLoading] = useState(false);

  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async (formData) => {
      
      try {
        if(formik.values.quantity<=0){
          Platform.OS === 'android'?
          ToastAndroid.show("La cantidad debe ser mayor a 0", ToastAndroid.SHORT):
          Toast.show("La cantidad debe ser mayor a 0", { position: Toast.positions.CENTER, });
        }else{
          const response = await agregarProducto(formData);
        if (response.statusCode===200) {
          Platform.OS === 'android'?
          ToastAndroid.show("Producto agregado a tu carrito", ToastAndroid.SHORT):
          Toast.show("Producto agregado a tu carrito", { position: Toast.positions.CENTER, });
          
        }else{
          Platform.OS === 'android'?
          ToastAndroid.show("La cantidad es mayor a la existencia", ToastAndroid.SHORT):
          Toast.show("La cantidad es mayor a la existencia", { position: Toast.positions.CENTER, });
          }
        }
      } catch (error) {
        alert("Error al intentar agregar producto: "+error)       
        setLoading(false);
      }
    },
    
  });

  return (
    
    <ScrollView style={{ width: "100%" }}>
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "#fff",
        }}
      >
        {/* Titulo principal */}

        <View style={{ flex: 1, padding: 10 }}>
          <Text
            style={{
              fontSize: 25,
              alignSelf: "center",
              color: "black",
              fontWeight: "bold",
              
            }}
          >
            Producto seleccionado
          </Text>
          {map(Index2, (Indexs2) => (
            <React.Fragment key={Indexs2.id}>
              <View
                style={{
                  alignItems: "center",
                  marginHorizontal: 1,
                  marginTop: 0,
                  paddingHorizontal: 1,
                  paddingVertical: 2,
                }}
              >
                {(() => {
                  if (Indexs2.image == null) {
                    return (
                      <Image
                        style={styles.image}
                        source={{
                          uri: `${"https://apprutas.kowi.com.mx/images/"}${"default.gif"}`,
                        }}
                      />
                    );
                  }
                  return (
                    <Image
                      style={styles.image}
                      source={{
                        uri: `${API_URL_IMAGES}${Indexs2.image}`,
                      }}
                    />
                  );
                })()}
                <Text
                  style={{
                    fontSize: 18,
                    alignSelf: "center",
                    color: "#000",
                    fontWeight: "700",
                    marginTop: 10,
                    paddingBottom: 0,
                    color:"#ff6900"
                  }}
                >
                  {Indexs2.nameproduct}
                </Text>
                <Text
                  style={{
                    fontSize: 12,
                    alignSelf: "center",
                    color: "#808080",
                    fontWeight: "700",
                    marginTop: 10,
                    paddingBottom: 15,
                  }}
                >
                  {Indexs2.nameproduct}
                </Text>

                {(() => {
                  if (Indexs1.preuab == 0) {
                    return (
                      <Text
                        style={{
                          fontSize: 12,
                          alignSelf: "center",
                          color: "#808080",
                          fontWeight: "700",
                          marginTop: 10,
                          paddingBottom: 15,
                        }}
                      >
                        Existencia: 0KG
                      </Text>
                    );
                  }
                  return (
                    <Text
                      style={{
                        fontSize: 12,
                        alignSelf: "center",
                        color: "#808080",
                        fontWeight: "700",
                        marginTop: 10,
                        paddingBottom: 15,
                      }}
                    >
                      Existencia: {Indexs1.preuab.existencia}KG
                    </Text>
                  );
                })()}
                <View
                  style={{
                    alignItems: "center",
                    marginHorizontal: 1,
                    marginTop: 0,
                    paddingHorizontal: 1,
                    paddingVertical: 2,
                    flexDirection: "row",
                    paddingBottom: 30,
                  }}
                >
                  <Text
                    style={{
                      fontSize: 35,
                      alignSelf: "center",
                      color: "#000",
                      fontWeight: "700",
                      paddingBottom: 0,
                      marginTop: "auto",
                      color:"#ff6900"
                    }}
                  >
                    {Indexs2.precios}
                  </Text>
                  <Text
                    style={{
                      fontSize: 18,
                      alignSelf: "center",
                      color: "#000",
                      fontWeight: "700",
                      marginTop: 10,
                      paddingBottom: 0,
                      color:"#ff6900"
                    }}
                  >
                    /KG
                  </Text>
                </View>
                {(() => {
                  if (Indexs1.preuab == 0) {
                    return (
                      <Text
                        style={{
                          fontSize: 16,
                          alignSelf: "flex-start",
                          color: "#E16161",
                          fontWeight: "700",
                          marginTop: 10,
                          paddingBottom: 0,
                        }}
                      >
                        Actualmente no hay existencias de este producto
                      </Text>
                    );
                  }
                  return (
                    <View>
                      {(() => {
                  if (Indexs1.preuab.existencia < 1) {
                    return (
                      <Text
                        style={{
                          fontSize: 16,
                          alignSelf: "flex-start",
                          color: "#E16161",
                          fontWeight: "700",
                          marginTop: 10,
                          paddingBottom: 0,
                        }}
                      >
                        Actualmente no hay existencias de este producto
                      </Text>
                    );
                  }
                  return (
                    <Text
                      style={{
                        fontSize: 18,
                        alignSelf: "flex-start",
                        color: "#000",
                        fontWeight: "700",
                        marginTop: 10,
                        paddingBottom: 0,
                      }}
                    >
                      Seleccione la cantidad que desea:
                    </Text>
                  );
                })()}
                    </View>
                  );
                })()}

                {(() => {
                  if (Indexs1.preuab == 0) {
                    return <Text></Text>;
                  }
                  return (
                    <View style={{ width: "100%", alignItems: "center" }}>
                      <TextInput
                        onChangeText={(text) =>
                          formik.setFieldValue("quantity", text)
                        }
                        keyboardType='numeric'
                        value={formik.values.quantity}
                        error={formik.errors.quantity}
                        placeholderTextColor="#4C4C4C"
                        style={{
                          borderWidth: 0.5,
                          borderColor: "#fff",
                          width: "90%",
                          marginTop: 0,
                          color: "#808080",
                          textAlign: "center",
                          marginBottom: 20,
                          borderWidth: 2,
                          borderBottomColor: "#808080",
                          fontSize: 22,
                        }}
                      />

                      <RadioButton.Group
                        onValueChange={(text) =>
                          formik.setFieldValue("uom", text)
                        }
                        value={formik.values.uom}
                        error={formik.errors.uom}
                      >
                        <View style={{ width: "100%", flexDirection: "row" }}>
                          <RadioButton value="KG"></RadioButton>
                          <Text style={{ fontSize: 24 }}>KG</Text>
                        </View>
                        <View style={{ width: "100%", flexDirection: "row" }}>
                          <RadioButton value="CAJA"></RadioButton>
                          <Text style={{ fontSize: 24 }}>Caja</Text>
                        </View>
                      </RadioButton.Group>

                      <TextInput
                        onChangeText={(text) =>
                          formik.setFieldValue("factor", text)
                        }
                        value={
                          (formik.values.factor =
                            Indexs1.products.factor.toString())
                        }
                        error={formik.errors.factor}
                        style={{
                          borderWidth: 0.5,
                          borderColor: "#808080",
                          width: "20%",
                          marginTop: -20,
                          color: "#808080",
                          textAlign: "center",
                          marginBottom: 40,
                          display: "none",
                        }}
                      />

                      <TextInput
                        onChangeText={(text) =>
                          formik.setFieldValue("pesopromedio", text)
                        }
                        value={
                          (formik.values.pesopromedio =
                            Indexs1.products.pesopromedio.toString())
                        }
                        error={formik.errors.pesopromedio}
                        style={{
                          borderWidth: 0.5,
                          borderColor: "#808080",
                          width: "20%",
                          marginTop: -20,
                          color: "#808080",
                          textAlign: "center",
                          marginBottom: 40,
                          display: "none",
                        }}
                      />
                      <TextInput
                        onChangeText={(text) =>
                          formik.setFieldValue("existencia", text)
                        }
                        value={
                          (formik.values.existencia =
                            Indexs1.preuab.existencia.toString())
                        }
                        error={formik.errors.existencia}
                        style={{
                          borderWidth: 0.5,
                          borderColor: "#808080",
                          width: "20%",
                          marginTop: -20,
                          color: "#808080",
                          textAlign: "center",
                          marginBottom: 40,
                          display: "none",
                        }}
                      />
                      <TextInput
                        onChangeText={(text) =>
                          formik.setFieldValue("id", text)
                        }
                        value={
                          (formik.values.id = Indexs1.cartanduser.id.toString())
                        }
                        error={formik.errors.id}
                        style={{
                          borderWidth: 0.5,
                          borderColor: "#808080",
                          width: "20%",
                          marginTop: -20,
                          color: "#808080",
                          textAlign: "center",
                          marginBottom: 40,
                          display: "none",
                        }}
                      />

                      <TextInput
                        onChangeText={(text) =>
                          formik.setFieldValue("product_id", text)
                        }
                        value={
                          (formik.values.product_id =
                            Indexs1.products.id.toString())
                        }
                        error={formik.errors.product_id}
                        style={{
                          borderWidth: 0.5,
                          borderColor: "#808080",
                          width: "20%",
                          marginTop: -20,
                          color: "#808080",
                          textAlign: "center",
                          marginBottom: 40,
                          display: "none",
                        }}
                      />

                      <TextInput
                        onChangeText={(text) =>
                          formik.setFieldValue("iddatprecio", text)
                        }
                        value={
                          (formik.values.iddatprecio = Indexs2.id.toString())
                        }
                        error={formik.errors.iddatprecio}
                        style={{
                          borderWidth: 0.5,
                          borderColor: "#808080",
                          width: "20%",
                          marginTop: -20,
                          color: "#808080",
                          textAlign: "center",
                          marginBottom: 40,
                          display: "none",
                        }}
                      />

                      <TextInput
                        onChangeText={(text) =>
                          formik.setFieldValue("precioprod", text)
                        }
                        value={
                          (formik.values.precioprod =
                            Indexs2.precios.toString())
                        }
                        error={formik.errors.precioprod}
                        style={{
                          borderWidth: 0.5,
                          borderColor: "#808080",
                          width: "20%",
                          marginTop: -20,
                          color: "#808080",
                          textAlign: "center",
                          marginBottom: 40,
                          display: "none",
                        }}
                      />
                    </View>
                  );
                })()}

                {(() => {
                  if (Indexs1.preuab == 0) {
                    return <Text></Text>;
                  }
                  return (
                    <View>
                      {(() => {
                  if (Indexs1.preuab.existencia < 1) {
                    return <Text></Text>;
                  }
                  return (
                    <View>
                      {(() => {
                        if (formik.values.quantity == "") {
                          return (
                            <View
                              style={{
                                alignItems: "center",
                                justifyContent: "center",
                                marginTop: 30,
                                backgroundColor: "#fff",
                                paddingVertical: 0,
                                borderRadius: 23,
                                width: 300,
                              }}
                            >
                              {/* Iniciar sesion */}
                              <TouchableOpacity
                                disabled={true}
                                style={styles.buttonagregar2}
                              >
                                <Text
                                  style={{
                                    fontSize: 24,
                                    color: "#fff",
                                    alignSelf: "center",
                                    paddingTop: 5,
                                    fontWeight: "700",
                                  }}
                                >
                                  Agregar
                                </Text>
                              </TouchableOpacity>
                            </View>
                          );
                        }
                        return (
                          <View>
                            {(() => {
                              if (formik.values.uom == "") {
                                return (
                                  <View
                                    style={{
                                      alignItems: "center",
                                      justifyContent: "center",
                                      marginTop: 30,
                                      backgroundColor: "#fff",
                                      paddingVertical: 0,
                                      borderRadius: 23,
                                      width: 300,
                                    }}
                                  >
                                    {/* Iniciar sesion */}
                                    
                                    <TouchableOpacity
                                      disabled={true}
                                      style={styles.buttonagregar2}
                                    >
                                      <Text
                                        style={{
                                          fontSize: 24,
                                          color: "#fff",
                                          alignSelf: "center",
                                          paddingTop: 5,
                                          fontWeight: "700",
                                        }}
                                      >
                                        AGREGAR
                                      </Text>
                                    </TouchableOpacity>
                                  </View>
                                );
                              }
                              return (
                                <View
                                  style={{
                                    alignItems: "center",
                                    justifyContent: "center",
                                    marginTop: 30,
                                    backgroundColor: "#fff",
                                    paddingVertical: 0,
                                    borderRadius: 23,
                                    width: 300,
                                  }}
                                >
                                  {/* Iniciar sesion */}
                                  <TouchableOpacity
                                    mode="contained"
                                    onPress={formik.handleSubmit}
                                    loading={setLoading}
                                    style={styles.buttonagregar}
                                   
                                  >
                                    <Text
                                      style={{
                                        fontSize: 24,
                                        color: "#fff",
                                        alignSelf: "center",
                                        paddingTop: 5,
                                        fontWeight: "700",
                                      }}
                                    >
                                      AGREGAR
                                    </Text>
                                  </TouchableOpacity>
                                </View>
                              );
                            })()}
                          </View>
                        );
                      })()}
                    </View>
                  );
                })()}
                    </View>
                  );
                })()}
              </View>
            </React.Fragment>
          ))}
        </View>

        {/* politica 1 */}
        <Text
          style={{
            fontSize: 11,
            color: "#5C5C5C",

            textAlign: "center",
            marginHorizontal: 55,
            marginTop: 30,
            opacity: 0.8,

            marginBottom: 0,
          }}
        >
          Alimentos Kowi 2021
        </Text>

        {/* politica 2 */}
        <Text
          style={{
            fontSize: 9,
            color: "#0083CA",

            textAlign: "center",
            marginHorizontal: 55,
            marginTop: 0,
            opacity: 0.8,
            marginBottom: 40,
          }}
        >
          Politica de privacidad
        </Text>
      </View>
    </ScrollView>
  );
}

function initialValues() {
  return {
    uom: "",
    factor: "",
    pesopromedio: "",
    existencia: "",
    quantity: "",
    id: "",
    product_id: "",
    iddatprecio: "",
    precioprod: "",
  };
}

function validationSchema() {
  return {
    uom: Yup.string().required(true),
    factor: Yup.string().required(true),
    pesopromedio: Yup.string().required(true),
    existencia: Yup.string().required(true),
    quantity: Yup.string().required(true),
    id: Yup.string().required(true),
    product_id: Yup.string().required(true),
    iddatprecio: Yup.string().required(true),
    precioprod: Yup.string().required(true),
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
    margin: -3,
  },
  containerProduct: {
    width: "100%",
    padding: 3,
  },
  product: {
    backgroundColor: "#f0f0f0",
    padding: 10,
  },
  image: {
    height: 250,
    width: 250,
  },
  name: {
    marginTop: 15,
    fontSize: 18,
  },
  buttonagregar: {
    backgroundColor: "#FF7709",
    borderRadius: 23,
    width: "100%",
    height: 40,
    marginBottom: 0,
  },
  buttonagregar2: {
    backgroundColor: "#FFC18F",
    borderRadius: 23,
    width: "100%",
    height: 40,
    marginBottom: 0,
  },
});
