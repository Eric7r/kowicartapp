import React from 'react'
import { View, Text } from 'react-native'
import Carousel from 'react-native-snap-carousel';
import { API_URL_IMAGES } from "../../utils/constanst";

export default function CarouselImages(props) {
    const {images} = props;
   
    console.log(images);

    const renderItem = ({item}) => {
        return <Image style={styles.carousel} source={{uri:`${API_URL_IMAGES}${item.image}`}} />
    }

    return (
        <>
        <Carousel 
        layout={"default"}
        data={images}
        sliderWidth={300}
        itemWidth={300}
        renderItem={renderItem}
        />
        </>
    )
}
